# egad-intra

## Requisites

Ubuntu 20.04

### Clone the repository

```
sudo apt install git
git clone https://gitlab.com/egadnet/egad-intra
```

### Install dotnet 3.1


#### Setup the 20.04 repositories

```
sudo wget https://packages.microsoft.com/config/ubuntu/20.04/packages-microsoft-prod.deb -O packages-microsoft-prod.deb
sudo dpkg -i packages-microsoft-prod.deb
```

#### Install the SDK

```
sudo apt-get update; \
  sudo apt-get install -y apt-transport-https && \
  sudo apt-get update && \
  sudo apt-get install -y dotnet-sdk-3.1
```
	
### Install SQL Server


#### Import the public repository GPG keys

```
wget --output-document=microsoft.asc https://packages.microsoft.com/keys/microsoft.asc
sudo apt-key add microsoft.asc
```

#### Register the Microsoft SQL Server Ubuntu repository for SQL Server 2019

```
sudo add-apt-repository "$(wget -qO- https://packages.microsoft.com/config/ubuntu/20.04/mssql-server-2019.list)"
```

#### Install SQL Server

```
sudo apt-get update
sudo apt-get install -y mssql-server
```

#### Configure the SA user for SQL Server.

```
sudo /opt/mssql/bin/mssql-conf setup
```

You will be asked:
1. The SQL Server edition. Select "2"
2. The language
3. The password

#### Check if the server is running

```
systemctl status mssql-server --no-pager
```

### Generating DataBase


#### Download dotnet-ef 

```
dotnet tool install --global dotnet-ef
export PATH="$PATH:/home/egadnet/.dotnet/tools"

cat << \EOF >> ~/.bash_profile
# Add .NET Core SDK tools
export PATH="$PATH:/home/egadnet/.dotnet/tools"
EOF
```

#### Restart the PC

```
reboot
```

#### Enter the project directory

```
cd egad-intra/Egad-Intra/Egad-Intra/
```

#### Generate the migrations and databases

```
dotnet ef migrations add -c HangfireContext hangfire
dotnet ef migrations add -c EgadContext egad
dotnet ef migrations add -c AuthDbContext authDb

dotnet ef database update -c HangfireContext
dotnet ef database update -c Egadcontext
dotnet ef database update -c AuthDbContext
```

### Running the application

```
cd bin/Debug/netcoreapp3.1/
dotnet Egad-Intra.dll
```


## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.

