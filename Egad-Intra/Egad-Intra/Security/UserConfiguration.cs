﻿using EgadIntra.Models;
using Microsoft.AspNet.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace EgadIntra.Security
{
    internal class UserConfiguration : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            var hash = new PasswordHasher();
            builder.HasData(new User
            {
                UserName = "Jonatan Gabriel Busch",
                Email = "jonatan.busch@egadnet.com.br",
                PasswordHash = hash.HashPassword("123456"),
            });
        }
    }
}
