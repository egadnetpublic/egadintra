using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System.Text;
using System;
using EgadIntra.Teste.ConsoleApp;
using System.Net.Mail;
using System.Net;
using System.Linq;
using Microsoft.Extensions.Configuration;


namespace EgadIntra.Models
{
    public class Employee : BaseModel
    {

        [Required]
        public string Name { get; set; }

        public string SocialName { get; set; }

        [Required]
        public string Birthday { get; set; }

        [Required]
        [RegularExpression("([0-9]{11})")]
        public string Cpf { get; set; }

        [Required]
        public string Eschooling { get; set; }

        [Required]
        public bool Deficiency { get; set; }

        [Required]
        public string DeficiencyType { get; set; }

        [Required]
        public string Gender { get; set; }

        [Required]
        public bool Stranger { get; set; }

        [Required]
        public string EntryCondition { get; set; }

        [Required]
        [RegularExpression("([0-9]{11})")]
        public string Pis { get; set; }

        [Required]
        public string Email { get; set; }

        [Required]
        public string CellPhone { get; set; }

        [Required]
        public string Telephone { get; set; }

        [Required]
        public string CivilianState { get; set; }

        [Required]
        public string RaceColor { get; set; }

        [Required]
        public int? Code { get; set; }

        public int? CorporationId { get; set; }

        public Corporation Corporation { get; set; }



        public void sendAnniversaryEmails()
        {
            using var context = new EgadContext();
            var builder = new ConfigurationBuilder().AddJsonFile("appsettings.json");
            var _configuration = builder.Build();


            List<Employee> employees = context.Employees.ToList();
            foreach (var employee in employees)
            {
                var anniversary = DateTime.Parse(employee.Birthday);
                var now = DateTime.Now;

                if (anniversary.Day != now.Day) continue;
                if (anniversary.Month != now.Month) continue;

                var smtpClient = new SmtpClient(_configuration["smtp:host"])
                {
                    Port = Convert.ToInt32(_configuration["smtp:port"]),
                    Credentials = new NetworkCredential(_configuration["smtp:user"], _configuration["smtp:password"]),
                    EnableSsl = true,
                };

                var mailMessage = new MailMessage
                {
                    From = new MailAddress("noreply@egadnet.com.br"),
                    Subject = "Aniversary",
                    Body = $"Congrats {employee.Name},\nYou are one year closer to die!! " +
                    $"\nEnjoy your {now.Year - anniversary.Year} years while it lasts!",
                };
                mailMessage.To.Add(employee.Email);

                smtpClient.Send(mailMessage);
            }
        }
        public bool CPFValidator()
        {
            
            int total = 0;
            int mult = 10;
            for (int i = 0; i < 9; i++)
            {
                total = total + (Cpf[i] - 48) * mult;
                mult--;
            }

            total = total % 11;
            if (total == 0 || total == 1)
            {
                if (0 != (Cpf[9] - 48)) return false;
            }
            else
            {
                if ((11 - total) != (Cpf[9] - 48)) return false;
            }


            total = 0;
            mult = 11;
            for (int i = 0; i < 10; i++)
            {

                total = total + ((Cpf[i] - 48) * mult);
                mult--;
            }

            total = total % 11;
            if (total == 0 || total == 1)
            {
                if (0 != (Cpf[10] - 48)) return false;
            }
            else
            {
                if ((11 - total) != (Cpf[10] - 48)) return false;
            }

            return true;
        }
    }
}
