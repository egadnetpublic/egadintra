﻿using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System.Text;
using System;

namespace EgadIntra.Models
{
    public class Corporation : BaseModel
    {
        [Required]
        public string Name { get; set; }

        [Required]
        [RegularExpression("([0-9]{14})")]
        public string Cnpj { get; set; }

        
        public override string ToString()
        {
            return $"{Name}, {Cnpj}";
        }

        public bool CNPJValidator()
        {

            int total = 0;
            int mult = 2;
            for (int i = 11; i >= 0; i--)
            {
                if (mult > 9) mult = 2;
                total = total + (Cnpj[i] - 48) * mult;
                mult++;
            }

            total = total % 11;
            if (total == 0 || total == 1)
            {
                if (0 != (Cnpj[12] - 48)) return false;
            }
            else
            {
                if ((11 - total) != (Cnpj[12] - 48)) return false;
            }

            total = 0;
            mult = 2;
            for (int i = 12; i >= 0; i--)
            {
                if (mult > 9) mult = 2;

                var pos = (Cnpj[i]);
                var valor = pos - 48;
                var novoTotal =(valor * mult);

                total += novoTotal;
                mult++;
            }

            total = total % 11;
            if (total == 0 || total == 1)
            {
                if (0 != (Cnpj[13] - 48)) return false;
            }
            else
            {
                if ((11 - total) != (Cnpj[13] - 48)) return false;
            }

            return true;
        }

    }
}
