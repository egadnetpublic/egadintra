﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace EgadIntra.Models
{
    public class DocumentApi
    {
        [Key]
        public string name { get; set; }
        public IFormFile Image { get; set; }
        public int EmployeeId { get; set; }

    }

    public class Document
    {
        public int Id { get; set; }

        public string Name { get; set; }
        
        public Byte[] Image { get; set; }
        
        public int EmployeeId { get; set; }
        
        public Employee Employee { get; set; }

    }
}
