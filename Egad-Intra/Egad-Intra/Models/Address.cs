﻿using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System.Text;
using System;

namespace EgadIntra.Models
{
    public class Address : BaseModel
    {
        [Required]
        public string Street { get; set; }

        [Required]
        public int? Number { get; set; }

        [Required]
        public string Complement { get; set; } 

        [Required]
        public string Neighborhood { get; set; } 

        [Required]
        public string City { get; set; }

        [Required]
        [RegularExpression("([0-9]{8})")]
        public string Cep { get; set; }

        [Required]
        public string AddressType { get; set; }

        [Required]
        public int EmployeeId { get; set; }

        public Employee Employee { get; set; }
        
        public override string ToString()
        {
            return $"{AddressType}: {Street}, nº{Number}, {Complement}; bairro {Neighborhood}, cep: {Cep}, {City}";
        }
    }
}
