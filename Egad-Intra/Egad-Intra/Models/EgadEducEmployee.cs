﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EgadIntra.Models
{
    class EgadEducEmployee
    {

        public string Birthday { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Login { get; set; }
        public string Document { get; set; }

    }
}
