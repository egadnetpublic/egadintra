﻿using System;
using System.Collections.Generic;
using System.Text;
using EgadIntra.Models;

namespace EgadIntra.Models
{
    public class CompleteEmployee
    {

        public Employee Employee { get; set; }

        public Address Address { get; set; }

        public List<Dependent> Dependents { get; set; }


    }
}
