﻿using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System.Text;
using System;

namespace EgadIntra.Models
{
    public class Dependent : BaseModel
    {

        [Required]
        public string Name { get; set; }

        [Required]
        public string Kinship { get; set; }

        [Required]
        public string BornDate { get; set; }

        [Required]
        public bool IrrfDep { get; set; }

        [Required]
        public int EmployeeId { get; set; }

        public Employee Employee { get; set; }

        public override string ToString()
        {
            return $"{Name}, born in {BornDate}";
        }

    }
}
