﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace EgadIntra.Models
{
    public class Enums
    {
        [Required]
        public string Table { get; set; }

        [Required]
        public string Field { get; set; }
        
        public int? Code { get; set; }

        public string Description { get; set; }
        
        public bool? Active { get; set; }

        public override string ToString()
        {
            return Description;
        }

    }
}
