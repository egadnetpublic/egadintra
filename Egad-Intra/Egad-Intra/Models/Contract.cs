﻿using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System.Text;
using System;
using EgadIntra.Teste.ConsoleApp;
using System.Linq;

namespace EgadIntra.Models
{
    public class Contract : BaseModel
    {

        [Required]
        public string Sector { get; set; }
        
        [Required]
        public bool Dangerousness { get; set; }
        
        [Required]
        public bool Unhealthiness { get; set; }

        [Required]
        public string Function { get; set; }
        
        [Required]
        public int Days { get; set; }
        
        [Required]
        public int Extendable { get; set; }
        
        [Required]
        public bool ExperienceContract { get; set; }

        [Required]
        public string AdmissionDate { get; set; }
        
        [Required]
        public int MonthlyJourney { get; set; }

        [Required]
        public string WorkSchedule { get; set; }
        
        [Required]
        public double Salary { get; set; }

        [Required]
        public string SalaryType { get; set; }
        
        [Required]
        public bool NightAddicional { get; set; }
        
        [Required]
        public bool TransportacionVoucher { get; set; }
        
        [Required]
        public bool FoodVoucher { get; set; }
        
        [Required]
        public double VoucherValue { get; set; }

        public double Months { get; set; }

        public double Years { get; set; }

        [Required]
        public int EmployeeId { get; set; }

        public Employee Employee { get; set; }

        public override string ToString()
        {
            return $"unhealthiness: {Unhealthiness}, Dangerousness: {Dangerousness}, salary: {Salary}, voucherValue: {VoucherValue}";
        }

        public void UpdateContracts()
        {
            using var context = new EgadContext();
            List<Contract> contracts = context.Contracts.ToList();
            foreach (var contract in contracts)
            {
                var admission = DateTime.Parse(contract.AdmissionDate);
                var now = DateTime.Now;
                var diference = now - admission;

                int leapYears = 0;
                for (var i = admission.Year; i < now.Year; i++)
                {
                    if (DateTime.IsLeapYear(i) == true) leapYears++;
                }
                decimal days = (decimal)diference.Days - leapYears;


                var difYear = Math.Round(days / 365, 2);
                var difMonth = Math.Round(difYear * 12, 2);

                contract.Years = (double)difYear;
                contract.Months = (double)difMonth;
            }

            context.SaveChanges();
        }

    }
}
