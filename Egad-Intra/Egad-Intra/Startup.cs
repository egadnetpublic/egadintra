﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using System;
using Microsoft.OpenApi.Models;
using Hangfire;
using EgadIntra.Controllers;

namespace EgadIntra.Teste.ConsoleApp
{
    public class Startup
    {
        public IConfiguration Configuration { get; }

        public Startup()
        {
            var builder = new ConfigurationBuilder().AddJsonFile("appsettings.json");
            Configuration = builder.Build();
        }


        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton(Configuration);

            //Hangfire
            var server = Configuration["ConnectionStrings:hangfireserver"];
            var user = Configuration["ConnectionStrings:user"];
            var pass = Configuration["ConnectionStrings:password"];
            var port = Convert.ToInt32(Configuration["ConnectionStrings:port"]);
            var database = Configuration["ConnectionStrings:hangfire"];

            services.AddHangfire(x => x.UseSqlServerStorage($"Server={server},{port};Database={database};User ID={user};Password={pass}"));
            services.AddHangfireServer();
            services.AddControllers();


            //mvc
            services.AddRouting();
            services.AddMvc().AddMvcOptions(options =>
            {
                options.EnableEndpointRouting = false;
            });


            //token config
            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = "JwtBearer";
                options.DefaultChallengeScheme = "JwtBearer";
            }).AddJwtBearer("JwtBearer", options =>
            {
                options.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuer = true,
                    ValidateAudience = true,
                    ValidateLifetime = true,
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(System.Text.Encoding.UTF8.GetBytes(Configuration["Token:key"])),
                    ClockSkew = TimeSpan.FromMinutes(5),
                    ValidIssuer = Configuration["Token:issuer"],
                    ValidAudience = Configuration["Token:audience"],

                };
            });


            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "EgadIntra", Description = "API documentacion", Version = "1.0" });
                c.EnableAnnotations();


                c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
                {
                    Name = "Authorization",
                    In = ParameterLocation.Header,
                    Type = SecuritySchemeType.ApiKey,
                    Description = "Autenticação Bearer via JWT"
                });

                c.AddSecurityRequirement(new OpenApiSecurityRequirement
                {
                    {
                        new OpenApiSecurityScheme
                        {
                            Reference = new OpenApiReference { Type = ReferenceType.SecurityScheme, Id = "Bearer" }
                        },
                        new string[] {  }
                    }
                });


            });

        }
        public void Configure(IApplicationBuilder app, IServiceProvider serviceProvider)
        {
            app.UseCors(builder => { builder.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader(); });
            app.UseDeveloperExceptionPage();

            app.UseStaticFiles();
            app.UseAuthentication();

            app.UseMvcWithDefaultRoute();

            app.UseSwagger();
            app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "v1"));


            app.UseHangfireDashboard("/mydashboard");
            StartJobs();
            
        }

        public void StartJobs()
        {
            var updateContract = new UpdateContractController();
            updateContract.Update();

            var birthdayMail = new BirthdayEmailController();
            birthdayMail.AnniversaryEmail();
        }
    }
}