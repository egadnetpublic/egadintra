﻿using Microsoft.AspNetCore.Hosting;

namespace EgadIntra.Teste.ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            IWebHost host = new WebHostBuilder()
                .UseKestrel()
                .UseStartup<Startup>()
                .UseUrls("http://0.0.0.0:5000")
                .Build();

            host.Run();
        }
    }
} 
