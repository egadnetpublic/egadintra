﻿using EgadIntra.Teste.ConsoleApp;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using EgadIntra.Models;
using System;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore;
using Swashbuckle.AspNetCore.Annotations;
using System.Linq;

namespace EgadIntra.Controllers
{
    [Authorize]
    [ApiController]
    [ApiExplorerSettings(GroupName = "v1")]
    [Route("Address")]
    public class AddressController : Controller
    {

        [HttpGet("GetAllAddresses")]
        [SwaggerOperation(Summary = "Gets all Addresses in the Database")]
        [ProducesResponseType(statusCode: 200, Type = typeof(List<Address>))]
        [ProducesResponseType(statusCode: 401)]
        [ProducesResponseType(statusCode: 500, Type = typeof(string))]
        [Produces("application/json")]
        public IActionResult GetAllAddresses()
        {
            try
            {
                var addresses = new EgadContext().Addresses.Include(a => a.Employee).ThenInclude(e => e.Corporation).ToList();
                return Ok(addresses);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }


        //
        [HttpGet("GetAddressById/{Id}")]
        [ProducesResponseType(statusCode: 200, Type = typeof(Address))]
        [ProducesResponseType(statusCode: 404)]
        [ProducesResponseType(statusCode: 401)]
        [ProducesResponseType(statusCode: 500, Type = typeof(string))]
        [SwaggerOperation(
            Summary = "Gets an Address by its Id",
            Description = "Requires a valid Address Id"
        )]
        [Produces("application/json")]
        public ActionResult GetAddressById([SwaggerParameter("The Id of the address")] int Id)
        {
            try
            {
                using var contexto = new EgadContext();
                var address = contexto.Addresses.Where(e => e.Id == Id)
                    .Include(a => a.Employee)
                    .ThenInclude(e => e.Corporation)
                    .FirstOrDefault();

                if (address != null) return Ok(address);

                return NotFound();
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        //
        [HttpPost("CreateAddress")]
        [SwaggerResponse(statusCode: 201, Type = typeof(Address), Description = "Created")]
        [ProducesResponseType(statusCode: 400)]
        [ProducesResponseType(statusCode: 401)]
        [ProducesResponseType(statusCode: 404)]
        [ProducesResponseType(statusCode: 500, Type = typeof(string))]
        [Produces("application/json")]
        [SwaggerOperation(
            summary: "Creates a new Address",
            description: "AddressType field is restricted and must be filled whith the respective Enum's Code. See GetEnum for more information."
            )]
        public ActionResult CreateAddress([FromBody] [SwaggerRequestBody("The Address that is going to be saved in the datebase. The Employee field is not required")] Address address)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    
                    using var context = new EgadContext();
                    var employee = context.Employees.Where(e => e.Id == address.EmployeeId).FirstOrDefault();
                    if (employee != null)
                    {
                        var type = context.Enums.Where(e => e.Table == "Address"
                            && e.Field == "AddressType"
                            && e.Active == true
                        ).ToList();

                        var addressType = type.FirstOrDefault(e => e.Code == Convert.ToInt32(address.AddressType));
                        if (addressType == null) return BadRequest("Enum not found");

                        address.AddressType = addressType.ToString();
                        
                        context.Addresses.Add(address);
                        context.SaveChanges();
                        return Created("", address);
                    }
                    return NotFound();
                }
                return BadRequest();
            }
            catch (FormatException ex)
            {
                //Possible cause: Field AddressType
                return StatusCode(500, $"{ex.Message} Possible cause: Field AddressType");
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        //
        [HttpDelete("DeleteAddress/{Id}")]
        [SwaggerOperation(Summary = "Deletes the Address with the given Id")]
        [SwaggerResponse(204, "No Content")]
        [ProducesResponseType(statusCode: 401)]
        [ProducesResponseType(statusCode: 404)]
        [ProducesResponseType(statusCode: 500, Type = typeof(string))]
        public ActionResult DeleteAddress([SwaggerParameter("The Id of the address")] int Id)
        {
            try
            {
                using var context = new EgadContext();

                var address = context.Addresses.Where(ad => ad.Id == Id).FirstOrDefault();

                if (address != null)
                {
                    context.Addresses.Remove(address);
                    context.SaveChanges();
                    return NoContent();
                }
                return NotFound();
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        //
        [HttpPut("UpdateAddress")]
        [SwaggerOperation(summary: "Updates the Address", 
            description: "AddressType field is restricted and must be filled whith the respective Enum's Code. See GetEnum for more information.")]
        [ProducesResponseType(statusCode: 200, type: typeof(Address))]
        [ProducesResponseType(statusCode: 401)]
        [ProducesResponseType(statusCode: 404)]
        [ProducesResponseType(statusCode: 500, Type = typeof(string))]
        [Produces("application/json")]
        public ActionResult UpdateAddress([FromBody] Address address)
        {
            try
            {
                using var context = new EgadContext();

                var addressDB = context.Addresses.Where(ad => ad.Id == address.Id).FirstOrDefault();

                if (addressDB != null)
                {
                    if (address.EmployeeId > 0)
                    {
                        var employeeDB = context.Employees.Where(e => e.Id == address.EmployeeId).FirstOrDefault();
                        if (employeeDB == null) return NotFound();

                        addressDB.EmployeeId = address.EmployeeId;
                        addressDB.Employee = address.Employee;
                    }

                    if (address.AddressType != null && address.AddressType != "") 
                    {
                        
                        var types = context.Enums.Where(
                            e => e.Table == "Address"
                            && e.Field == "AddressType"
                            && e.Active == true
                        ).ToList();
                        
                        var addressType = types.FirstOrDefault(e => e.Code == Convert.ToInt32(address.AddressType));
                        if (addressType == null) return BadRequest("Enum not found");
                        
                        addressDB.AddressType = addressType.ToString();
                    } 

                    if (address.City != null && address.City != "null") addressDB.City = address.City;
                    if (address.Cep != null && address.Cep != "null") addressDB.Cep = address.Cep;
                    if (address.Complement != null && address.Complement != "null") addressDB.Complement = address.Complement;
                    if (address.Neighborhood != null && address.Neighborhood != "null") addressDB.Neighborhood = address.Neighborhood;
                    if (address.Number != 0) addressDB.Number = address.Number;
                    if (address.Street != null && address.Street != "null") addressDB.Street = address.Street;

                    context.Update(address);
                    context.SaveChanges();
                    return Ok(addressDB);
                }
                return NotFound();
            }
            catch (FormatException ex)
            {
                return StatusCode(500, $"{ex.Message} Possible cause: Field AddressType");
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }
    }
}
