﻿using EgadIntra.Teste.ConsoleApp;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using EgadIntra.Models;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using Swashbuckle.AspNetCore.Annotations;
using System;

namespace EgadIntra.Controllers
{
    [Authorize]
    [ApiController]
    [Route("Employee")]
    [ApiExplorerSettings(GroupName = "v1")]
    public class EmployeeController : Controller
    {
        enum Gender { Masculine, Feminine, Other };

        [HttpGet("GetAllEmployees")]
        [SwaggerOperation("Gets all Employees in the Database")]
        [SwaggerResponse(200, Type = typeof(List<Employee>))]
        [SwaggerResponse(401)]
        [ProducesResponseType(statusCode: 500, Type = typeof(string))]
        [Produces("application/json")]
        public ActionResult GetAllEmployees()
        {
            try
            {
                var employees = new EgadContext().Employees.ToList();
                return Ok(employees);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        [HttpGet("GetEmployeeById/{Id}")]
        [SwaggerOperation("Gets an Employee by its Id")]
        [SwaggerResponse(200, Type = typeof(Employee))]
        [SwaggerResponse(401)]
        [SwaggerResponse(404)]
        [ProducesResponseType(statusCode: 500, Type = typeof(string))]
        [Produces("application/json")]
        public ActionResult GetEmployeeById([SwaggerParameter("The Id of the Employee")] int Id)
        {
            try
            {
                using var contexto = new EgadContext();
                var employee = contexto.Employees
                    .Where(e => e.Id == Id)
                    .Include(e => e.Corporation)
                    .FirstOrDefault();

                if (employee != null) return Ok(employee);

                return NotFound();
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        [HttpPost("CreateEmployee")]
        [SwaggerOperation("Creates a new Employee", "Gender, Eschooling, DeficiencyType, EntryCondition, CivilianState and RaceColor fields are restricted and must be filled whith the respective Enum's Code. See GetEnum for more information.")]
        [SwaggerResponse(201, "Created", typeof(Employee))]
        [SwaggerResponse(400)]
        [SwaggerResponse(401)]
        [SwaggerResponse(404)]
        [ProducesResponseType(statusCode: 500, Type = typeof(string))]
        [Produces("application/json")]
        public ActionResult CreateEmployee([FromBody] Employee employee)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    using var contexto = new EgadContext();

                    var validationResult = IsValidEmployee(employee);
                    if (validationResult != null) return BadRequest(validationResult);
                    if (!employee.CPFValidator()) return BadRequest("Invalid CPF");

                    

                    var corporation = contexto.Corporations.Where(e => e.Id == employee.CorporationId).FirstOrDefault();
                    if (corporation != null)
                    {
                        if (employee.Stranger == false) employee.EntryCondition = "0";
                        if (employee.Deficiency == false) employee.DeficiencyType = "0";

                        var enums = contexto.Enums.Where(e => e.Table == "Employee" && e.Active == true).ToList();

                        var gender = enums.FirstOrDefault(e => e.Field == "Gender"
                            && e.Code == Convert.ToInt32(employee.Gender));
                        if (gender == null) return BadRequest("Enum Not Found");
                        employee.Gender = gender.ToString();

                        var eschooling = enums.FirstOrDefault(e => e.Field == "Eschooling"
                            && e.Code == Convert.ToInt32(employee.Eschooling));
                        if (eschooling == null) return BadRequest("Enum Not Found");
                        employee.Eschooling = eschooling.ToString();

                        var civilState = enums.FirstOrDefault(e => e.Field == "CivilianState"
                            && e.Code == Convert.ToInt32(employee.CivilianState));
                        if (civilState == null) return BadRequest("Enum Not Found");
                        employee.CivilianState = civilState.ToString();

                        var DeficiencyType = enums.FirstOrDefault(e => e.Field == "DeficiencyType"
                            && e.Code == Convert.ToInt32(employee.DeficiencyType));
                        if (DeficiencyType == null) return BadRequest("Enum Not Found");
                        employee.DeficiencyType = DeficiencyType.ToString();

                        var RaceColor = enums.FirstOrDefault(e => e.Field == "RaceColor"
                            && e.Code == Convert.ToInt32(employee.RaceColor));
                        if (RaceColor == null) return BadRequest("Enum Not Found");
                        employee.RaceColor = RaceColor.ToString();

                        var EntryCondicion = enums.FirstOrDefault(e => e.Field == "EntryCondicion"
                            && e.Code == Convert.ToInt32(employee.EntryCondition));
                        if (EntryCondicion == null) return BadRequest("Enum Not Found");
                        employee.EntryCondition = EntryCondicion.ToString();

                        contexto.Employees.Add(employee);
                        contexto.SaveChanges();
                        
                        //return RedirectToAction("/SendEmployee", "Rabbit", employee);
                        return Ok();
                    }
                    return NotFound();
                }
                catch (FormatException ex)
                {
                    return StatusCode(500, $"{ex.Message} Possible cause: Fields Eschooling, EntryCondicion, RaceColor, DeficiencyType, CivilianState, Gender");
                }
                catch (Exception ex)
                {
                    return StatusCode(500, ex.Message);
                }
            }
            return BadRequest();
        }

        [HttpDelete("DeleteEmployee/{Id}")]
        [SwaggerOperation("Deletes the Employee with the given Id")]
        [SwaggerResponse(204, "No Content")]
        [SwaggerResponse(401)]
        [SwaggerResponse(404)]
        [ProducesResponseType(statusCode: 500, Type = typeof(string))]
        [Produces("application/json")]
        public ActionResult DeleteEmployee([SwaggerParameter("The Id of Employee")]int Id)
        {
            try
            {
                using var context = new EgadContext();

                var employee = context.Employees.Where(ad => ad.Id == Id).FirstOrDefault();

                if (employee != null)
                {
                    context.Employees.Remove(employee);
                    context.SaveChanges();
                    return NoContent();
                }
                return NotFound();
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        [HttpPut("UpdateEmployee")]
        [SwaggerOperation("Updates the Employee", "Gender, Eschooling, DeficiencyType, EntryCondition, CivilianState and RaceColor fields are restricted and must be filled whith the respective Enum's Code. See GetEnum for more information.")]
        [SwaggerResponse(200, Type = typeof(Employee))]
        [SwaggerResponse(400)]
        [SwaggerResponse(401)]
        [SwaggerResponse(404)]
        [ProducesResponseType(statusCode: 500, Type = typeof(string))]
        [Produces("application/json")]
        public ActionResult UpdateEmployee([FromBody] Employee employee)
        {
            try
            {
                var ValidEmployee = IsValidEmployee(employee);
                if (ValidEmployee != null) return BadRequest(ValidEmployee);

                using var context = new EgadContext();
                var employeeDB = context.Employees.Where(ad => ad.Id == employee.Id).FirstOrDefault();



                if (employeeDB != null)
                {

                    if (employee.CorporationId != 0)
                    {
                        var corporationDB = context.Corporations.Where(e => e.Id == employee.CorporationId).FirstOrDefault();
                        if (corporationDB == null) return NotFound();

                        employeeDB.Corporation = corporationDB;
                        employeeDB.CorporationId = corporationDB.Id;
                    }

                    if (employee.Cpf != "" && employee.Cpf != null)
                    {
                        if (employee.CPFValidator())
                        {
                            employeeDB.Cpf = employee.Cpf;
                        }
                        else
                        {
                            return BadRequest();
                        }
                    }
                    

                    var ens = context.Enums.Where(e => e.Table == "Employee" && e.Active == true).ToList();
                    if (employee.Gender != null && employee.Gender != "") 
                    {
                        var gender = ens.FirstOrDefault(e => e.Field == "Gender" && e.Code == Convert.ToInt32(employee.Gender));
                        if (gender == null) return BadRequest("Enum Not Found");
                        employeeDB.Gender = gender.ToString();
                    }

                    if (employee.Eschooling != null && employee.Eschooling != "")
                    {
                        var eschooling = ens.FirstOrDefault(e => e.Field == "Eschooling" && e.Code == Convert.ToInt32(employee.Eschooling));
                        if (eschooling == null) return BadRequest("Enum Not Found");
                        employeeDB.Eschooling = eschooling.ToString();
                    }

                    if (employee.CivilianState != null && employee.CivilianState != "")
                    {
                        var civilState = ens.FirstOrDefault(e => e.Field == "CivilianState" && e.Code == Convert.ToInt32(employee.CivilianState));
                        if (civilState == null) return BadRequest("Enum Not Found");
                        employeeDB.CivilianState = civilState.ToString();
                    }

                    if (employee.DeficiencyType != null && employee.DeficiencyType != "") 
                    {
                        var DeficiencyType = ens.FirstOrDefault(e => e.Field == "DeficiencyType" && e.Code == Convert.ToInt32(employee.DeficiencyType));
                        if (DeficiencyType == null) return BadRequest("Enum Not Found");
                        employeeDB.DeficiencyType = DeficiencyType.ToString();
                    }

                    if (employee.RaceColor != null && employee.RaceColor != "")
                    {
                        var RaceColor = ens.FirstOrDefault(e => e.Field == "RaceColor" && e.Code == Convert.ToInt32(employee.RaceColor));
                        if (RaceColor == null) return BadRequest("Enum Not Found");
                        employeeDB.RaceColor = RaceColor.ToString();
                    }

                    if (employee.EntryCondition != null && employee.EntryCondition != "")
                    {
                        var EntryCondicion = ens.FirstOrDefault(e => e.Field == "EntryCondicion" && e.Code == Convert.ToInt32(employee.EntryCondition));
                        if (EntryCondicion == null) return BadRequest("Enum Not Found");
                        employeeDB.EntryCondition = EntryCondicion.ToString();
                    }

                    if (employee.CellPhone != "" && employee.CellPhone != null) employeeDB.CellPhone = employee.CellPhone;
                    if (employee.Name != "" && employee.Name != null) employeeDB.Name = employee.Name;
                    if (employee.SocialName != "" && employee.SocialName != null) employeeDB.SocialName = employee.SocialName;
                    if (employee.Deficiency != employeeDB.Deficiency) employeeDB.Deficiency = employee.Deficiency;
                    if (employee.Stranger != employeeDB.Stranger) employeeDB.Stranger = employee.Stranger;
                    if (employee.Pis != "" && employee.Pis != null) employeeDB.Pis = employee.Pis;
                    if (employee.Email != "" && employee.Email != null) employeeDB.Email = employee.Email;
                    if (employee.Telephone != "" && employee.Telephone != null) employeeDB.Telephone = employee.Telephone;
                    if (employee.Code != 0) employeeDB.Code = employee.Code;


                    context.SaveChanges();
                    return Ok(employeeDB);
                }
                return NotFound();
            }
            catch (FormatException ex)
            {
                return StatusCode(500, $"{ex.Message} Possible cause: Fields Eschooling, EntryCondicion, RaceColor, DeficiencyType, CivilianState, Gender");
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        [HttpPost("Register")]
        [SwaggerOperation(
            "Register a new employee, Address and Dependent", 
            "Creates a new Employee, its Address and its Dependents. " +
            "Gender, Eschooling, DeficiencyType, EntryCondition, CivilianState and RaceColor from Employee, AddressType from Address and Kinship from Dependent fields are restricted and must be filled whith the respective Enum's Code. See GetEnum for more information.")]
        [SwaggerResponse(200)]
        [SwaggerResponse(400)]
        [SwaggerResponse(401)]
        [SwaggerResponse(404)]
        [ProducesResponseType(statusCode: 500, Type = typeof(string))]
        [Produces("application/json")]
        public ActionResult RegisterFull([FromBody] CompleteEmployee completeEmployee)
        {
            try
            {
                var employee = completeEmployee.Employee;
                var address = completeEmployee.Address;
                var dependents = completeEmployee.Dependents;


                if (!ModelState.IsValid)
                {
                    return BadRequest();
                }


                //Validation
                var validEmployee = IsValidEmployee(employee);
                if (validEmployee != null) return BadRequest(validEmployee);
                if (!employee.CPFValidator()) return BadRequest("Invalid CPF");

                using var context = new EgadContext();

                var corp = context.Corporations.Where(c => c.Id == employee.CorporationId).FirstOrDefault();
                if (corp == null) return NotFound("Corporation Not Found");


                //Emp
                if (employee.Deficiency == false) employee.DeficiencyType = "0";
                if (employee.Stranger == false) employee.EntryCondition = "0";

                var enums = context.Enums.Where(e => e.Active == true).ToList();
                var EmployeeEnums = enums.Where(e => e.Table == "Employee");

                var gender = EmployeeEnums.FirstOrDefault(e => e.Field == "Gender"
                    && e.Code == Convert.ToInt32(employee.Gender));
                if (gender == null) return BadRequest("Gender Enum Not Found");
                employee.Gender = gender.ToString();

                var eschooling = EmployeeEnums.FirstOrDefault(e => e.Field == "Eschooling"
                    && e.Code == Convert.ToInt32(employee.Eschooling));
                if (eschooling == null) return BadRequest("Eschooling Enum Not Found");
                employee.Eschooling = eschooling.ToString();

                var civilState = EmployeeEnums.FirstOrDefault(e => e.Field == "CivilianState"
                    && e.Code == Convert.ToInt32(employee.CivilianState));
                if (civilState == null) return BadRequest("CivilianState Enum Not Found");
                employee.CivilianState = civilState.ToString();

                var DeficiencyType = EmployeeEnums.FirstOrDefault(e => e.Field == "DeficiencyType"
                    && e.Code == Convert.ToInt32(employee.DeficiencyType));
                if (DeficiencyType == null) return BadRequest("DeficiencyType Enum Not Found");
                employee.DeficiencyType = DeficiencyType.ToString();

                var RaceColor = EmployeeEnums.FirstOrDefault(e => e.Field == "RaceColor"
                    && e.Code == Convert.ToInt32(employee.RaceColor));
                if (RaceColor == null) return BadRequest("RaceColor Enum Not Found");
                employee.RaceColor = RaceColor.ToString();

                var EntryCondicion = EmployeeEnums.FirstOrDefault(e => e.Field == "EntryCondicion"
                    && e.Code == Convert.ToInt32(employee.EntryCondition));
                if (EntryCondicion == null) return BadRequest("EntryCondicion Enum Not Found");
                employee.EntryCondition = EntryCondicion.ToString();

                context.Employees.Add(employee);


                //Address
                address.Employee = employee;

                var addressType = enums.FirstOrDefault(
                    e => e.Table == "Address" 
                    && e.Field == "AddressType"
                    && e.Code == Convert.ToInt32(address.AddressType));
                if (addressType == null) return BadRequest("AddressType Enum Not Found");
                address.AddressType = addressType.ToString();

                context.Addresses.Add(address);


                //Depen
                foreach (var dependent in dependents)
                {
                    dependent.Employee = employee;
                    
                    var Kinship = enums.FirstOrDefault(
                        e => e.Table == "Dependent"
                        && e.Field == "Kinship"
                        && e.Code == Convert.ToInt32(dependent.Kinship)
                    );
                    if (Kinship == null) return BadRequest("Kinship Enum not found");
                    dependent.Kinship = Kinship.ToString();

                    context.Dependents.Add(dependent);
                }


                context.SaveChanges();
                return Ok();
            }
            catch (FormatException ex)
            {
                return StatusCode(500, $"{ex.Message} Possible cause: Employee Fields Eschooling, EntryCondicion, RaceColor, DeficiencyType, CivilianState, Gender; Address Field AddressType; Dependent field Kinship");
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }









        private string IsValidEmployee(Employee employee)
        {
            using var contexto = new EgadContext();
            var employees = contexto.Employees.ToList();
            foreach (var emp in employees)
            {
                if (emp.Id == employee.Id) continue;
                if (emp.Cpf == employee.Cpf) return $"The Cpf is already in Database";
                if (emp.Pis == employee.Pis) return "The Pis is already in Database";
                if (emp.Email == employee.Email) return "The Email is already in Database";
                if (emp.Code == employee.Code) return "The Code is already in Database";
            }
            return null;
        }
    }

    
}
