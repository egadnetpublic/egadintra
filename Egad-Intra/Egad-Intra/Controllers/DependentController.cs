﻿using EgadIntra.Teste.ConsoleApp;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using EgadIntra.Models;
using System.Linq;
using System;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore;
using Swashbuckle.AspNetCore.Annotations;

namespace EgadIntra.Controllers
{
    [Authorize]
    [ApiController]
    [Route("Dependent")]
    [ApiExplorerSettings(GroupName = "v1")]
    public class DependentController : Controller
    {

        [HttpGet("GetAllDependents")]
        [SwaggerOperation("Gets all Dependents in the Database")]
        [Produces("application/json")]
        [SwaggerResponse(200, Type = typeof(List<Dependent>))]
        [SwaggerResponse(401)]
        [ProducesResponseType(statusCode: 500, Type = typeof(string))]
        public IActionResult GetAllDependents()
        {
            try
            {
                var dependents = new EgadContext().Dependents.Include(d => d.Employee).ThenInclude(e => e.Corporation).ToList();
                return Ok(dependents);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }
        
        [HttpGet("GetDependentById/{Id}")]
        [SwaggerResponse(200, Type = typeof(Dependent))]
        [SwaggerResponse(401)]
        [SwaggerResponse(404)]
        [ProducesResponseType(statusCode: 500, Type = typeof(string))]
        [SwaggerOperation("Gets an Dependent by its Id")]
        [Produces("application/json")]
        public ActionResult GetDependentById([SwaggerParameter("The Id of the Dependent")] int Id)
        {
            try
            {
                using var contexto = new EgadContext();
                var dependent = contexto.Dependents
                    .Include(d => d.Employee)
                    .ThenInclude(e => e.Corporation)
                    .Where(e => e.Id == Id)
                    .FirstOrDefault();

                if (dependent != null)
                {
                    return Ok(dependent);
                }

                return NotFound();
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        [HttpPost("CreateDependent")]
        [SwaggerOperation("Creates a new Dependent", "Kinship field is restricted and must be filled whith the respective Enum's Code. See GetEnum for more information.")]
        [SwaggerResponse(201, "Created", Type = typeof(Dependent))]
        [SwaggerResponse(400)]
        [SwaggerResponse(401)]
        [SwaggerResponse(404)]
        [ProducesResponseType(statusCode: 500, Type = typeof(string))]
        [Produces("application/json")]
        public ActionResult CreateDependent([FromBody] Dependent dependent)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    using var context = new EgadContext();

                    var employee = context.Employees.Where(e => e.Id == dependent.EmployeeId).FirstOrDefault();
                    if (employee != null)
                    {
                        var enums = context.Enums.Where(
                            e => e.Table == "Dependent"
                            && e.Field == "Kinship"
                            && e.Active == true
                        ).ToList();

                        var kinship = enums.FirstOrDefault(e => e.Code == Convert.ToInt32(dependent.Kinship));
                        if (kinship == null) return BadRequest("Enum not found");

                        dependent.Kinship = kinship.ToString();

                        context.Dependents.Add(dependent);
                        context.SaveChanges();
                        return Created("", dependent);
                    }
                    return NotFound();
                }
                return BadRequest();
            }
            catch (FormatException ex)
            {
                return StatusCode(500, $"{ex.Message} Possible cause: Field Kinship");
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        [HttpDelete("DeleteDependent/{Id}")]
        [SwaggerOperation("Deletes the Dependent with the given Id")]
        [SwaggerResponse(204, "No Content")]
        [SwaggerResponse(401)]
        [SwaggerResponse(404)]
        [ProducesResponseType(statusCode: 500, Type = typeof(string))]
        public ActionResult DeleteDependent([SwaggerParameter("The Id of the Dependent")] int Id)
        {
            try
            {
                using var context = new EgadContext();

                var dependent = context.Dependents.Where(ad => ad.Id == Id).FirstOrDefault();

                if (dependent != null)
                {
                    context.Dependents.Remove(dependent);
                    context.SaveChanges();
                    return NoContent();
                }
                return NotFound();
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        [HttpPut("UpdateDependent")]
        [SwaggerOperation("Updates the Dependent", "Kinship field is restricted and must be filled whith the respective Enum's Code. See GetEnum for more information.")]
        [SwaggerResponse(200, Type = typeof(Dependent))]
        [SwaggerResponse(400)]
        [SwaggerResponse(401)]
        [SwaggerResponse(404)]
        [ProducesResponseType(statusCode: 500, Type = typeof(string))]
        [Produces("application/json")]
        public ActionResult UpdateDependent([FromBody] Dependent  dependent)
        {
            try
            {
                using var context = new EgadContext();

                var dependentDB = context.Dependents.Where(ad => ad.Id == dependent.Id).FirstOrDefault();



                if (dependentDB != null)
                {
                    if (dependent.EmployeeId != 0)
                    {
                        var employeeDB = context.Employees.Where(e => e.Id == dependent.EmployeeId).FirstOrDefault();
                        if (employeeDB == null) return NotFound();

                        dependentDB.Employee = employeeDB;
                        dependentDB.EmployeeId = employeeDB.Id;

                    }

                    if (dependent.Name != null && dependent.Name != "") dependentDB.Name = dependent.Name;
                    
                    if (dependent.Kinship != null && dependent.Kinship != "")
                    {
                        var enums = context.Enums.Where(
                            e => e.Table == "Dependent"
                            && e.Field == "Kinship"
                            && e.Active == true
                        ).ToList();

                        var kinship = enums.FirstOrDefault(e => e.Code == Convert.ToInt32(dependent.Kinship));
                        if (kinship == null) return BadRequest("Enum not found");

                        dependentDB.Kinship = kinship.ToString();
                    }

                    if (dependent.BornDate != null && dependent.BornDate != "") dependentDB.BornDate = dependent.BornDate;

                    if (dependent.IrrfDep != dependentDB.IrrfDep) dependentDB.IrrfDep = dependent.IrrfDep;


                    context.SaveChanges();
                    return Ok(dependentDB);
                }
                return NotFound();
            }
            catch (FormatException ex)
            {
                return StatusCode(500, $"{ex.Message} Possible cause: Field Kinship");
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }
    }
}
