using EgadIntra.Models;
using EgadIntra.Teste.ConsoleApp;
using Hangfire;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Swashbuckle.AspNetCore.Annotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;

namespace EgadIntra.Controllers
{

    [Authorize]
    [Route("Hangfire")]
    [ApiController]
    public class BirthdayEmailController : Controller
    {

        [HttpGet("AnniversaryEmail")]
        [SwaggerOperation("Sends emails to the employees")]
        [SwaggerResponse(200)]
        [SwaggerResponse(401)]
        [SwaggerResponse(500, type: typeof(string))]
        public IActionResult AnniversaryEmail()
        {
            try
            {
                RecurringJob.AddOrUpdate(() => new Employee().sendAnniversaryEmails(), Cron.Daily(9), TimeZoneInfo.Local);
                return Ok();
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        
    }
}
