﻿using EgadIntra.Models;
using EgadIntra.Teste.ConsoleApp;
using Hangfire;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Collections.Generic;
using Swashbuckle.AspNetCore.Annotations;
using Microsoft.AspNetCore.Authorization;

namespace EgadIntra.Controllers
{
    [Authorize]
    [Route("Hangfire")]
    [ApiController]
    public class UpdateContractController : Controller
    {

        [HttpGet("UpdateContracts")]
        [SwaggerOperation("Updates the fields Months and Years of every Contract")]
        [SwaggerResponse(200)]
        [SwaggerResponse(401)]
        [SwaggerResponse(500, type: typeof(string))]
        public IActionResult Update()
        {
            try
            {
                RecurringJob.AddOrUpdate(() => new Contract().UpdateContracts(), Cron.Daily(1), TimeZoneInfo.Local);
                return Ok();
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        
    }
}
