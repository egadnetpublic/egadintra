﻿using EgadIntra.Teste.ConsoleApp;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using EgadIntra.Models;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using Swashbuckle.AspNetCore.Annotations;
using Microsoft.EntityFrameworkCore;
using System;

namespace EgadIntra.Controllers
{
    [Authorize]
    [ApiController]
    [ApiExplorerSettings(GroupName = "v1")]
    [Route("Contract")]
    public class ContractController : Controller
    {

        [HttpGet("GetAllContracts")]
        [SwaggerOperation("Gets all Contracts in the Datebase")]
        [ProducesResponseType(statusCode: 200, Type = typeof(List<Contract>))]
        [ProducesResponseType(statusCode: 401)]
        [ProducesResponseType(statusCode: 500, Type = typeof(string))]
        [Produces("application/json")]
        public IActionResult GetAllContracts()
        {
            try
            {
                var contracts = new EgadContext().Contracts.Include(c => c.Employee).ThenInclude(e => e.Corporation).ToList();
                return Ok(contracts);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        [HttpGet("GetContractById/{Id}")]
        [SwaggerOperation("Get an Contract by its Id")]
        [ProducesResponseType(statusCode:200, Type = typeof(Contract))]
        [ProducesResponseType(statusCode:401)]
        [ProducesResponseType(statusCode:404)]
        [ProducesResponseType(statusCode: 500, Type = typeof(string))]
        [Produces("application/json")]
        public ActionResult GetContractById([SwaggerParameter("The Id of the Contract")] int Id)
        {
            try
            {
                using var contexto = new EgadContext();
                var contract = contexto.Contracts.Where(c => c.Id == Id).FirstOrDefault();
                if (contract != null)
                {
                    return Ok(contract);
                }
                return NotFound();
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        [HttpPost("CreateContract")]
        [SwaggerResponse(statusCode:201, Type = typeof(Contract), Description = "Created")]
        [ProducesResponseType(statusCode:400)]
        [ProducesResponseType(statusCode:401)]
        [ProducesResponseType(statusCode:404)]
        [ProducesResponseType(statusCode: 500, Type = typeof(string))]
        [Produces("application/json")]
        [SwaggerOperation(
            summary: "Creates a new Contract",
            description: "SalaryType field is restricted and must be filled whith the respective Enum's Code. See GetEnum for more information."
        )]
        public ActionResult CreateContract([FromBody] [SwaggerRequestBody("The contract that is going to be saved in the datebase. The Employee field is not required")] Contract contract)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    using var contexto = new EgadContext();
                    var employee = contexto.Employees.Where(e => e.Id == contract.EmployeeId).FirstOrDefault();
                    if (employee != null)
                    {
                        var enums = contexto.Enums.Where(
                            e => e.Table == "Contract"
                            && e.Field == "SalaryType" 
                            && e.Active == true
                        ).ToList();

                        var salaryType = enums.FirstOrDefault(e => e.Code == Convert.ToInt32(contract.SalaryType));
                        if (salaryType == null) return BadRequest("Enum not found");

                        contract.SalaryType = salaryType.ToString();

                        contexto.Contracts.Add(contract);
                        contexto.SaveChanges();
                        return Created("", contract);
                    }
                    return NotFound();
                }
                return BadRequest();
            }
            catch (FormatException ex)
            {
                return StatusCode(500, $"{ex.Message} Possible cause: Field SalaryType");
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        [HttpDelete("DeleteContract/{Id}")]
        [SwaggerResponse(statusCode: 204, description: "No Content")]
        [ProducesResponseType(statusCode: 401)]
        [ProducesResponseType(statusCode: 404)]
        [ProducesResponseType(statusCode: 500, Type = typeof(string))]
        [SwaggerOperation("Deletes the Contract with the given Id")]
        public ActionResult DeleteContract([SwaggerParameter("The Id of the Contract")] int Id)
        {
            try
            {
                using var context = new EgadContext();

                var contract = context.Contracts.Where(c => c.Id == Id).FirstOrDefault();

                if (contract != null)
                {
                    context.Contracts.Remove(contract);
                    context.SaveChanges();
                    return NoContent();
                }
                return NotFound();
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        [HttpPut("UpdateContract")]
        [SwaggerOperation(summary: "Updates the Contract", 
            description: "SalaryType field is restricted and must be filled whith the respective Enum's Code. See GetEnum for more information.")]
        [ProducesResponseType(statusCode: 200, Type = typeof(Contract))]
        [ProducesResponseType(statusCode: 400)]
        [ProducesResponseType(statusCode: 401)]
        [ProducesResponseType(statusCode: 404)]
        [ProducesResponseType(statusCode: 500, Type = typeof(string))]
        [Produces("application/json")]
        public ActionResult UpdateContract([FromBody] Contract contract)
        {
            try
            {
                using var context = new EgadContext();

                var contractDB = context.Contracts.Where(c => c.Id == contract.Id).FirstOrDefault();



                if (contractDB != null)
                {
                    if (contract.EmployeeId != 0)
                    {
                        var employeeDB = context.Employees.Where(e => e.Id == contract.EmployeeId).FirstOrDefault();
                        if (employeeDB == null)
                        {
                            return NotFound();
                        }
                        contractDB.Employee = employeeDB;
                        contractDB.EmployeeId = employeeDB.Id;

                    }

                    if (contract.AdmissionDate != null || contract.AdmissionDate != "") contractDB.AdmissionDate = contract.AdmissionDate;
                    if (contract.Function != null || contract.Function != "") contractDB.Function = contract.Function;
                    if (contract.Sector != null || contract.Sector != "") contractDB.Sector = contract.Sector;
                    if (contract.WorkSchedule != null || contract.WorkSchedule != "") contractDB.WorkSchedule = contract.WorkSchedule;

                    if (contract.SalaryType != null && contract.SalaryType != "")
                    {
                        var enums = context.Enums.Where(
                            e => e.Table == "Contract"
                            && e.Field == "SalaryType"
                            && e.Active == true
                        ).ToList();

                        var salaryType = enums.FirstOrDefault(e => e.Code == Convert.ToInt32(contract.SalaryType));
                        if (salaryType == null) return BadRequest("Enum not found");

                        contractDB.SalaryType = salaryType.ToString();
                    }

                    if (contract.Dangerousness != contractDB.Dangerousness) contractDB.Dangerousness = contract.Dangerousness;
                    if (contract.ExperienceContract != contractDB.ExperienceContract) contractDB.ExperienceContract = contract.ExperienceContract;
                    if (contract.FoodVoucher != contractDB.FoodVoucher) contractDB.FoodVoucher = contract.FoodVoucher;
                    if (contract.NightAddicional != contractDB.NightAddicional) contractDB.NightAddicional = contract.NightAddicional;
                    if (contract.TransportacionVoucher != contractDB.TransportacionVoucher) contractDB.TransportacionVoucher = contract.TransportacionVoucher;
                    if (contract.Unhealthiness != contractDB.Unhealthiness) contractDB.Unhealthiness = contract.Unhealthiness;

                    if (contract.Days != 0) contractDB.Days = contract.Days;
                    if (contract.Extendable != 0) contractDB.Extendable = contract.Extendable;
                    if (contract.MonthlyJourney != 0) contractDB.MonthlyJourney = contract.MonthlyJourney;
                    if (contract.Salary != 0) contractDB.Salary = contract.Salary;
                    if (contract.VoucherValue != 0) contractDB.VoucherValue = contract.VoucherValue;


                    context.SaveChanges();
                    return Ok(contractDB);
                }
                return NotFound();
            }
            catch (FormatException ex)
            {
                return StatusCode(500, $"{ex.Message} Possible cause: Field SalaryType");
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }
    }
}
