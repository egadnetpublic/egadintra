﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json;
using EgadIntra.Models;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Mvc;
using RabbitMQ.Client;
using EgadIntra.Teste.ConsoleApp;
using Swashbuckle.AspNetCore.Annotations;
using Microsoft.AspNetCore.Authorization;

namespace EgadIntra.Controllers
{
    [Authorize]
    [Route("Rabbit")]
    [ApiController]
    public class RabbitController : Controller
    {

        [HttpGet("SendEmployee")]
        [SwaggerOperation("Sends the Employee to RabbitMQ")]
        [SwaggerResponse(200)]
        [SwaggerResponse(401)]
        [SwaggerResponse(500, type: typeof(string))]
        public IActionResult SendEmployee(Employee emp)
        {

            var builder = new ConfigurationBuilder().AddJsonFile("appsettings.json");
            var _configuration = builder.Build();

            var factory = new ConnectionFactory();
            factory.UserName = _configuration["rabbit:user"];
            factory.Password = _configuration["rabbit:password"];
            factory.Port = Convert.ToInt32(_configuration["rabbit:port"]);
            factory.HostName = _configuration["rabbit:host"];
            factory.VirtualHost = _configuration["rabbit:vhost"];

            try
            {
                using (var contexto = new EgadContext())
                using (var connection = factory.CreateConnection())
                using (var channel = connection.CreateModel())
                {

                    var exchangeArguments = new Dictionary<string, object>();
                    exchangeArguments.Add("alternate-exchange", "orders-alternate-exchange");
                    channel.ExchangeDeclare("orders-direct-exchange", ExchangeType.Direct, true, false, exchangeArguments);
                    
                    /* campos restantes:
                     
                     '-' code / permissão
                     
                    */

                    var egadEducEmp = new EgadEducEmployee();
                    egadEducEmp.Login = emp.Name.Split(' ')[0].ToLower();
                    egadEducEmp.Document = string.Concat(string.Concat(emp.Cpf.Split('.')).Split('-'));
                    egadEducEmp.Birthday = emp.Birthday;
                    egadEducEmp.Email = emp.Email;
                    egadEducEmp.Name = emp.Name;


                    var json = JsonSerializer.Serialize(egadEducEmp);
                    var body = Encoding.UTF8.GetBytes(json);

                    channel.BasicPublish("orders-direct-exchange", "create", null, body);


                    return Ok();
                }
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }
    }
}
