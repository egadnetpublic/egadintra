﻿using EgadIntra.Teste.ConsoleApp;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using EgadIntra.Models;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using Swashbuckle.AspNetCore.Annotations;
using System;

namespace EgadIntra.Controllers
{

    [Authorize]
    [ApiController]
    [Route("Corporation")]
    [ApiExplorerSettings(GroupName = "v1")]
    public class CorporationController : Controller
    {

        [HttpGet("GetAllCorporations")]
        [SwaggerOperation("Gets all Corporations in the Database")]
        [ProducesResponseType(statusCode: 200, Type = typeof(List<Corporation>))]
        [ProducesResponseType(statusCode: 401)]
        [Produces("application/json")]
        public IActionResult GetAllCorporations()
        {
            try
            {
                var corporations = new EgadContext().Corporations.ToList();
                return Ok(corporations);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        [HttpGet("GetCorporationById/{Id}")]
        [ProducesResponseType(statusCode: 200, Type = typeof(Corporation))]
        [ProducesResponseType(statusCode: 401)]
        [ProducesResponseType(statusCode: 404)]
        [Produces("application/json")]
        [SwaggerOperation("Get a Corporation by its Id")]
        public ActionResult GetCorporationById([SwaggerParameter("The Id of the Corporation")] int Id)
        {
            try
            {
                using var contexto = new EgadContext();
                var corporation = contexto.Corporations.Where(c => c.Id == Id).FirstOrDefault();
                if (corporation != null) return Ok(corporation);

                return NotFound();
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        [HttpPost("CreateCorporation")]
        [SwaggerResponse(201, "Created", typeof(Corporation))]
        [SwaggerResponse(400)]
        [SwaggerResponse(401)]
        [SwaggerResponse(404)]
        [SwaggerOperation("Creates a new Corporation")]
        [Produces("application/json")]
        public ActionResult CreateCorporation([FromBody] Corporation corporation)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (!corporation.CNPJValidator())
                    {
                        return BadRequest();
                    }

                    using var contexto = new EgadContext();

                    if (contexto.Corporations.Where(c => c.Cnpj == corporation.Cnpj).FirstOrDefault() != null)
                    {
                        return BadRequest();
                    }

                    contexto.Corporations.Add(corporation);
                    contexto.SaveChanges();
                    return Created("", corporation);
                }

                return BadRequest();
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        [HttpDelete("DeleteCorporation/{Id}")]
        [SwaggerResponse(204, "No Content")]
        [SwaggerResponse(401)]
        [SwaggerResponse(404)]
        [SwaggerOperation("Deletes the Corporation with the given Id")]
        [Produces("application/json")]
        public ActionResult DeleteCorporation([SwaggerParameter("The id of the Corporation")] int Id)
        {
            try
            {
                using var context = new EgadContext();

                var corporation = context.Corporations.Where(c => c.Id == Id).FirstOrDefault();

                if (corporation != null)
                {
                    context.Corporations.Remove(corporation);
                    context.SaveChanges();
                    return NoContent();
                }
                return NotFound();
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        [HttpPut("UpdateCorporation")]
        [SwaggerResponse(200, Type = typeof(Corporation))]
        [SwaggerResponse(400)]
        [SwaggerResponse(401)]
        [SwaggerResponse(404)]
        [SwaggerOperation("Updates the Corporation")]
        [Produces("application/json")]
        public ActionResult UpdateCorporation([FromBody] Corporation corporation)
        {
            try
            {
                using var context = new EgadContext();

                var corporationDB = context.Corporations.Where(c => c.Id == corporation.Id).FirstOrDefault();

                if (corporationDB == null)
                {
                    return NotFound();
                }

                if (corporation.Cnpj != null && corporation.Cnpj != "")
                {
                    var corp = context.Corporations.Where(c => c.Cnpj == corporation.Cnpj).FirstOrDefault();
                    if (!corporation.CNPJValidator() || (corp != null && corporation.Cnpj != corporationDB.Cnpj)) return BadRequest();
                    corporationDB.Cnpj = corporation.Cnpj;

                }

                if (corporation.Name != null && corporation.Name != "null") corporationDB.Name = corporation.Name;

                context.Corporations.Update(corporationDB);
                context.SaveChanges();
                return Ok(corporationDB);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }
    }
}
