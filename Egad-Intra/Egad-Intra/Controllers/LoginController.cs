﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using EgadIntra.Security;
using EgadIntra.Views.ViewModel;
using Swashbuckle.AspNetCore.Annotations;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Linq;
using Microsoft.Extensions.Configuration;

namespace EgadIntra.Controllers
{
    [Route("Login")]
    [ApiController]
    [ApiExplorerSettings(GroupName = "v1")]
    public class LoginController : Controller
    {
        [HttpPost("Login")]
        [SwaggerResponse(200, Type = typeof(string))]
        [SwaggerResponse(400)]
        [SwaggerResponse(401)]
        [SwaggerOperation(summary: "Creates the JWT")]
        public IActionResult Login([FromBody] LoginViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {

                    var builder = new ConfigurationBuilder().AddJsonFile("appsettings.json");
                    var conf = builder.Build();

                    using var context = new AuthDbContext();
                    var hash = new PasswordHasher<User>();
                    var userDB = context.Users.Where(u => u.Email == model.Email).FirstOrDefault();
                    if (userDB == null) return Unauthorized();

                    var LoginResult = Convert.ToInt32(hash.VerifyHashedPassword(userDB, userDB.PasswordHash, model.Password));
                    if (LoginResult == 1)
                    {
                        var claims = new[]
                        {
                        new Claim(JwtRegisteredClaimNames.Sub, model.Login),
                        new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString())
                    };

                        var key = new SymmetricSecurityKey(System.Text.Encoding.UTF8.GetBytes(conf["Token:key"]));
                        var credentials = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

                        var token = new JwtSecurityToken(
                            issuer: conf["Token:issuer"],
                            audience: conf["Token:audience"],
                            claims: claims,
                            signingCredentials: credentials,
                            expires: DateTime.Now.AddHours(5)
                        );

                        var tokenString = new JwtSecurityTokenHandler().WriteToken(token);
                        return Ok(tokenString);
                    }
                    return Unauthorized();
                }
                return BadRequest();
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }


        [HttpPost("Create")]
        [ProducesResponseType(statusCode: 201, Type = typeof(User))]
        [SwaggerResponse(400)]
        [SwaggerOperation(Summary = "Creates a new User", Description = "The Email field must be unique")]
        public ActionResult Create([FromBody] LoginViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var context = new AuthDbContext();

                    if (context.Users.Where(u => u.Email == model.Email).FirstOrDefault() != null) return BadRequest();

                    var user = new User() { UserName = model.Login, Email = model.Email };
                    var hash = new PasswordHasher<User>();

                    user.PasswordHash = hash.HashPassword(user, model.Password);

                    context.Users.Add(user);
                    context.SaveChanges();
                    return Created("", user);
                }
                return BadRequest();
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }
    }
}