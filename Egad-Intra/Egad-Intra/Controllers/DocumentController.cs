﻿using System;
using System.IO;
using System.Linq;
using System.Net.Http;
using EgadIntra.Models;
using EgadIntra.Teste.ConsoleApp;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;

namespace EgadIntra.Controllers
{
    [Authorize]
    [ApiController]
    [Route("Document")]
    [ApiExplorerSettings(GroupName = "v1")]
    public class DocumentController : Controller
    {

        [HttpPost("Upload")]
        [SwaggerOperation("Uploads a document to the Database")]
        [SwaggerResponse(201, "Created", typeof(Document))]
        [SwaggerResponse(400)]
        [SwaggerResponse(401)]
        [SwaggerResponse(404)]
        public ActionResult Upload([FromForm] DocumentApi document)
        {
            try
            {
                if (document.Image.ContentType != "image/png")
                {
                    return BadRequest("Invalid file format");
                }

                HttpContent content = Create(document);



                using var context = new EgadContext();
                var employee = context.Employees.Where(e => e.Id == document.EmployeeId).FirstOrDefault();
                if (employee == null) return NotFound();

                var documento = new Document();
                documento.Name = document.name;
                documento.EmployeeId = document.EmployeeId;
                documento.Image = ConvertToBytes(document.Image);
                documento.Employee = employee;

                context.Documents.Add(documento);
                context.SaveChanges();

                return Created("", documento);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        [HttpGet("GetDocumentById/{Id}")]
        [SwaggerOperation("Gets a Document by its Id")]
        [SwaggerResponse(200, Type = typeof(FileContentResult))]
        [SwaggerResponse(401)]
        [SwaggerResponse(404)]
        public ActionResult GetDocumentById([SwaggerParameter("The Id of the Document")] int Id)
        {
            try
            {
                using var context = new EgadContext();
                var image = context.Documents.Where(d => d.Id == Id).Select(d => d.Image).FirstOrDefault();

                if (image == null) return NotFound();
                return File(image, "image/png");
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }





        private HttpContent Create(DocumentApi document)
        {
            var content = new MultipartFormDataContent();
            content.Add(new StringContent(document.name), "\"name\"");

            if(document.Image != null)
            {
                var imageContent = new ByteArrayContent(ConvertToBytes(document.Image));
                imageContent.Headers.Add("content-type", "image/png");
                content.Add(imageContent, "\"capa\"");
            }
            return content;
        }

        private static byte[] ConvertToBytes(IFormFile image)
        {
            if (image == null)
            {
                return null;
            }
            using (var inputStream = image.OpenReadStream())
            using (var stream = new MemoryStream())
            {
                inputStream.CopyTo(stream);
                return stream.ToArray();
            }
        }
    }
}
