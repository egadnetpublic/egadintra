﻿using EgadIntra.Teste.ConsoleApp;
using EgadIntra.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using Swashbuckle.AspNetCore.Annotations;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;

namespace EgadIntra.Controllers
{
    [Authorize]
    [ApiController]
    [Route("Enums")]
    public class EnumsController : Controller
    {

        [HttpGet("GetEnums/{table}/{field?}")]
        [SwaggerOperation("Get every Enum for that table. Tables: Employee, Contract, Address, Dependent",
            "Enums are the accepted values for the specified field. If no field if especified by the request, it will get every Enum for the Table")]
        [SwaggerResponse(statusCode: 200, Type = typeof(List<Enums>))]
        [SwaggerResponse(401)]
        [SwaggerResponse(500, Type = typeof(string))]
        [Produces("application/json")]
        public IActionResult getEnums(string table, [SwaggerParameter(Required = false)] string field)
        {
            try
            {
                using var context = new EgadContext();
                List<Enums> enums = null;
                if (field == null || field ==",") enums = context.Enums.Where(e => e.Table == table).ToList();
                else enums = context.Enums.Where(e => e.Table == table && e.Field == field).ToList();

                return Ok(enums);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        [HttpPost("AddEnum")]
        [SwaggerOperation("Adds a new Enum for the field of the database", 
            "The Enum will restringe the values for that field. The Code auto-increments and the default value for Active field is true.")]
        [SwaggerResponse(statusCode: 201, Type = typeof(Enums))]
        [SwaggerResponse(statusCode: 400, Type = typeof(string))]
        [SwaggerResponse(401)]
        [SwaggerResponse(statusCode: 500, Type = typeof(string))]
        [Produces("application/json")]
        public IActionResult add([FromBody] Enums enums)
        {
            try
            {
                if (!ModelState.IsValid || enums.Description == null) return BadRequest("The Table, Field and Description fields are Required");

                using var context = new EgadContext();

                if(enums.Code == null)
                {
                    var enumList = context.Enums.Where(e => e.Table == enums.Table && e.Field == enums.Field).ToList();
                    var index = enumList.Count() - 1;
                    if (index < 0) enums.Code = 0;
                    else { 
                        var DBenum = enumList[index];
                        enums.Code = DBenum.Code + 1;
                    }
                }
                else
                {
                    var DBenums = context.Enums.Where
                        (e => e.Table == enums.Table 
                            && e.Code == enums.Code 
                            && e.Field == enums.Field
                        ).FirstOrDefault();

                    if (DBenums != null) return BadRequest("Invalid Code");
                }

                if (enums.Active == null) enums.Active = true;

                context.Enums.Add(enums);
                context.SaveChanges();
                return Created("", enums);
            }
            catch (DbUpdateException ex)
            {
                return BadRequest($"{ex.Message} \nDuplicate value on Database");
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        [HttpPut("SwitchActive")]
        [SwaggerOperation("Updates the Enum",
            "This method switches the Active field from `true` to `false` or `false` to `true`. \nTable, Field and Code fields are required")]
        [ProducesResponseType(statusCode: 201, Type = typeof(Enums))]
        [ProducesResponseType(statusCode: 401)]
        [ProducesResponseType(statusCode: 404)]
        [ProducesResponseType(statusCode: 500, type: typeof(string))]
        [Produces("application/json")]
        public IActionResult edit([FromBody] Enums enums)
        {
            try
            {
                using var context = new EgadContext();
                var enumDB = context.Enums.FirstOrDefault(e => e.Code == enums.Code && enums.Field == e.Field && e.Table == enums.Table);
                if (enumDB == null) return NotFound();
                enumDB.Active = !enumDB.Active;

                context.SaveChanges();

                return Ok(enumDB);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }
    }
}
