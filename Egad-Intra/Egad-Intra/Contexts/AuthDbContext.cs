﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;

namespace EgadIntra.Security
{
    public class AuthDbContext : DbContext
    {
        public DbSet<User> Users { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            modelBuilder.ApplyConfiguration(new UserConfiguration());

        }


        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var builder = new ConfigurationBuilder().AddJsonFile("appsettings.json");
            var conf = builder.Build();

            var server = conf["ConnectionStrings:WinServer"];
            var user = conf["ConnectionStrings:user"];
            var pass = conf["ConnectionStrings:password"];
            var port = Convert.ToInt32(conf["ConnectionStrings:port"]);
            var database = conf["ConnectionStrings:authDB"];

            optionsBuilder.UseSqlServer($"Server={server},{port};Database={database};User ID={user};Password={pass}");
            // Server=localhost,1433;Database=EgadIntraAuth;User ID=SA;Password=123456a.
            // Server={server},{port};Database={database};User ID={user};Password={pass}
            // Server=(localdb)\\MSSQLLocalDB;Database=EgadIntraAuth;Trusted_Connection=True
            // Server={server};Database={database};Trusted_Connection=True
        }
    }
}
