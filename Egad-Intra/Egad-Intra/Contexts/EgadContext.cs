﻿using Microsoft.EntityFrameworkCore;
using EgadIntra.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using System;

namespace EgadIntra.Teste.ConsoleApp
{
    public class EgadContext : DbContext
    {
        public DbSet<Corporation> Corporations { get; set; }
        public DbSet<Employee> Employees { get; set; }
        public DbSet<Dependent> Dependents { get; set; }
        public DbSet<Contract> Contracts { get; set; }
        public DbSet<Address> Addresses { get; set; }
        public DbSet<Document> Documents { get; set; }
        public DbSet<Enums> Enums { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //chave composta
            modelBuilder
                .Entity<Enums>()
                .HasKey(pp => new { pp.Table, pp.Field, pp.Code });

            //modelBuilder
            //    .Entity<Endereco>()
            //    .ToTable("Enderecos");

            //modelBuilder
            //    .Entity<Address>()
            //    .Property<int>("ClienteId");

            //modelBuilder
            //    .Entity<Endereco>()
            //    .HasKey("ClienteId");

        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var builder = new ConfigurationBuilder().AddJsonFile("appsettings.json");
            var conf = builder.Build();

            var server = conf["ConnectionStrings:WinServer"];
            var user = conf["ConnectionStrings:user"];
            var pass = conf["ConnectionStrings:password"];
            var port = Convert.ToInt32(conf["ConnectionStrings:port"]);
            var database = conf["ConnectionStrings:EgadContext"];

            optionsBuilder.UseSqlServer($"Server={server},{port};Database={database};User ID={user};Password={pass}");
        }
    }
}