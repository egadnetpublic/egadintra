﻿using Microsoft.EntityFrameworkCore;
using EgadIntra.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using System;

namespace EgadIntra.Teste.ConsoleApp
{
    public class HangfireContext : DbContext
    {

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var builder = new ConfigurationBuilder().AddJsonFile("appsettings.json");
            var conf = builder.Build();

            var server = conf["ConnectionStrings:WinServer"];
            var user = conf["ConnectionStrings:user"];
            var pass = conf["ConnectionStrings:password"];
            var port = Convert.ToInt32(conf["ConnectionStrings:port"]);
            var database = conf["ConnectionStrings:Hangfire"];

            optionsBuilder.UseSqlServer($"Server={server},{port};Database={database};User ID={user};Password={pass}");
        }
    }
}