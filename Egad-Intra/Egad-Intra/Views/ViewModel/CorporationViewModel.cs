﻿using EgadIntra.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace EgadIntra.Views.ViewModel
{
    public class CorporationViewModel
    {
        [Required]
        public string Name { get; set; }
        [Required]
        public string Cnpj { get; set; }

        /*gerar as outras viewModel e fazer as valizacoes delas*/
    }
}
